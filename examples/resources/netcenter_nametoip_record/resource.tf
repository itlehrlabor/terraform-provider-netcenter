# Example IPv4 Record
resource "netcenter_nametoip_record" "ipv4record" {
  name = "test-marc-v4.ethz.ch"
  ip_address = "129.132.179.226"
  forward_dns = true
  reverse_dns = false
  ttl = 3200
  enable_dhcp = false
  enable_external_view = false
  enable_internal_view = true
  isg_group = "isg-group"
  ipversion = "v4"
  enable_letsencrypt = false
}

# Example IPv6 Record
resource "netcenter_nametoip_record" "ipv6record" {
  name = "test-marc.ethz.ch"
  ip_address = "2001:67c:10ec:4945::300"
  forward_dns = true
  reverse_dns = false
  ttl = 3200
  enable_dhcp = false
  enable_external_view = false
  enable_internal_view = true
  isg_group = "isg-group"
  ipversion = "v6"
  enable_letsencrypt = false
}

# This output will not show anything, if Let's Encrypt is enabled. Because the ACME API Key
# is considered sensitive.
output "ipv4record" {
  sensitive = false
  value = netcenter_nametoip_record.ipv4record
}

output "ipv6record" {
  sensitive = false
  value = netcenter_nametoip_record.ipv6record
}