resource "netcenter_alias_record" "aliasrecord" {
  alias_name = "test-marc-alias.ethz.ch"
  hostname = "target-hostname.ethz.ch"
  isg_group = "isg-group"
  enable_external_view = false
}

output "aliasrecord" {
  value = netcenter_alias_record.aliasrecord
}