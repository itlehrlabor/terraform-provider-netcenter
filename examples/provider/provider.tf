terraform {
  required_providers {
    netcenter = {
      source  = "ethz.ch/edu/netcenter"
    }
  }
}


provider "netcenter" {
  # example configuration here
  host     = "https://[netcenter_url]/netcenter/rest" # API URL of Netcenter Production
  username = "[ETHZ Username with permissions in Netcenter]" # Should be set through env Variable - NETCENTER_USERNAME
  password = "[ETHZ Password]" # Should be set through env variable / NETCENTER_PASSWORD
}