package provider

import (
	"strings"

	"github.com/hashicorp/terraform-plugin-sdk/v2/helper/schema"
	"github.com/hashicorp/terraform-plugin-sdk/v2/helper/validation"
)

func resourceNetcenterNameToIpRecordSchema() map[string]*schema.Schema {
	return map[string]*schema.Schema{
		"name": {
			Type:     schema.TypeString,
			Required: true,
			ForceNew: true,
			StateFunc: func(i interface{}) string {
				return strings.ToLower(i.(string))
			},
			Description: "Hostname in fqdn format (host.example.com).",
		},
		"ipversion": {
			Type:         schema.TypeString,
			Required:     true,
			ForceNew:     true,
			ValidateFunc: validation.StringInSlice([]string{"v4", "v6"}, true),
			Description:  "Please set the IP Version to either v4 or v6, depending on the NameToIp Ip Address.",
		},
		"ip_address": {
			Type:        schema.TypeString,
			Required:    true,
			ForceNew:    true,
			Description: "IP(v4 or v6) Adress in Format x.x.x.x or x:x:x:x:x the record should point to.",
			ValidateFunc: validation.Any(
				validation.IsIPAddress,
			),
		},
		"forward_dns": {
			Type:        schema.TypeBool,
			Optional:    true,
			ForceNew:    true,
			Description: "Set if the Record should be added to the Fordward DNS Zone. Default true",
			Default:     true,
		},
		"reverse_dns": {
			Type:        schema.TypeBool,
			Optional:    true,
			ForceNew:    true,
			Description: "Set if a reverse DNS Zone record should be added. Please be aware: Only one Reverse Record per IP is allowed! This will throw an error if there already exists a Reverse IP Record. Default false",
			Default:     false,
		},
		"ttl": {
			Type:        schema.TypeInt,
			Optional:    true,
			Computed:    true,
			ForceNew:    true,
			Description: "Set the TTL of the forward Record in seconds. Default is based on Zone TTL.",
		},
		"enable_dhcp": {
			Type:        schema.TypeBool,
			Optional:    true,
			ForceNew:    true,
			Default:     false,
			Description: "Enable or disable DHCP on Record. Optional: Set MAC Address in dhcp_mac_reservation. Default false",
		},
		"dhcp_mac_reservation": {
			Type:        schema.TypeString,
			Optional:    true,
			ForceNew:    false,
			Computed:    true,
			Description: "Optional: Set MAC Address in dhcp_mac_reservation.",
		},
		"ip_last_detection": {
			Type:        schema.TypeString,
			Computed:    true,
			Description: "Last Detection of IP Address on the Network.",
		},
		"enable_external_view": {
			Type:        schema.TypeBool,
			Optional:    true,
			Default:     false,
			ForceNew:    true,
			Description: "Set true, if you want the Record to be avaiable outside of ETH. Only possible for public Ips. Default false",
		},
		"enable_internal_view": {
			Type:        schema.TypeBool,
			Optional:    true,
			Default:     true,
			ForceNew:    true,
			Description: "Set true, if you want the Record to be avaiable inside of ETH. Default True",
		},
		"isg_group": {
			Type:        schema.TypeString,
			Required:    true,
			ForceNew:    true,
			Description: "Set NetSupport group for the record. You must be a member of this group.",
		},
		"enable_letsencrypt": {
			Type:        schema.TypeBool,
			Optional:    true,
			Default:     false,
			ForceNew:    true,
			Description: "Set this to true if you want to enable Let's Encrypt, Default is false.",
		},
		"acme_base_url": {
			Type:        schema.TypeString,
			Computed:    true,
			Description: "Base URL for the ETH Let's Encrypt implementation.",
		},
		"acme_username": {
			Type:        schema.TypeString,
			Computed:    true,
			Description: "Acme Username as defined when Let's Encrypt is enabled in Netcenter.",
		},
		"acme_password": {
			Type:        schema.TypeString,
			Computed:    true,
			Description: "Acme Password as defined when Let's Encrypt is enabled in Netcenter.",
			Sensitive:   true,
		},
		"acme_subdomain": {
			Type:        schema.TypeString,
			Computed:    true,
			Description: "Acme Subdomain as defined when Let's Encrypt is enabled in Netcenter.",
		},
		"last_updated": {
			Type:     schema.TypeString,
			Computed: true,
		},
	}
}
