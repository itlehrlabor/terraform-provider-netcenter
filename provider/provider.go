package provider

import (
	"context"

	"github.com/hashicorp/terraform-plugin-sdk/v2/diag"
	"github.com/hashicorp/terraform-plugin-sdk/v2/helper/schema"
	nc "gitlab.ethz.ch/itlehrlabor/netcenter-client-go/api"
)

// Provider -
func Provider() *schema.Provider {
	return &schema.Provider{
		Schema: map[string]*schema.Schema{
			"host": {
				Type:        schema.TypeString,
				Required:    true,
				DefaultFunc: schema.EnvDefaultFunc("NETCENTER_HOSTNAME", nil),
				Description: "URL of the Netcenter API, Production URL is: https://www.netcenter.ethz.ch/netcenter/rest. Can also be set through env Var: NETCENTER_HOSTNAME",
			},
			"username": {
				Type:        schema.TypeString,
				Required:    true,
				DefaultFunc: schema.EnvDefaultFunc("NETCENTER_USERNAME", nil),
				Description: "Usrname with adequate permissions in Netcenter. Can also be set through env Variable: NETCENTER_USERNAME",
			},
			"password": {
				Type:        schema.TypeString,
				Required:    true,
				Sensitive:   true,
				DefaultFunc: schema.EnvDefaultFunc("NETCENTER_PASSWORD", nil),
				Description: "Password of Netcenter User, should be set through env Variable: NETCENTER_PASSWORD",
			},
		},
		ResourcesMap: map[string]*schema.Resource{
			"netcenter_nametoip_record": resourceNetcenterRecord(),
			"netcenter_alias_record":    resourceNetcenterAliasRecord(),
		},
		DataSourcesMap: map[string]*schema.Resource{
			//"hashicups_nametoip_records": dataSourceNetcenterIPSubnet(),
		},
		ConfigureContextFunc: providerConfigure,
	}
}

// providerConfigure supplies necessary configuration for the Netcenter provider to work. Hostname, Username, Password for example.
func providerConfigure(ctx context.Context, d *schema.ResourceData) (interface{}, diag.Diagnostics) {
	username := d.Get("username").(string)
	password := d.Get("password").(string)

	var host *string

	hVal, ok := d.GetOk("host")
	if ok {
		tempHost := hVal.(string)
		host = &tempHost
	}

	// Warning or errors can be collected in a slice type
	var diags diag.Diagnostics

	if (username != "") && (password != "") {
		c, err := nc.NewClient(host, &username, &password)
		if err != nil {
			diags = append(diags, diag.Diagnostic{
				Severity: diag.Error,
				Summary:  "Unable to create Netcenter client",
				Detail:   "Unable to authenticate user for authenticated Netcenter client",
			})

			return nil, diags
		}

		return c, diags
	}

	c, err := nc.NewClient(host, nil, nil)
	if err != nil {
		diags = append(diags, diag.Diagnostic{
			Severity: diag.Error,
			Summary:  "Unable to create Netcenter client",
			Detail:   "Unable to create anonymous Netcenter client",
		})
		return nil, diags
	}

	return c, diags
}
