package provider

import (
	"strings"

	"github.com/hashicorp/terraform-plugin-sdk/v2/helper/schema"
)

func resourceNetcenterAliasRecordSchema() map[string]*schema.Schema {
	return map[string]*schema.Schema{
		"alias_name": {
			Type:     schema.TypeString,
			Required: true,
			ForceNew: true,
			StateFunc: func(i interface{}) string {
				return strings.ToLower(i.(string))
			},
			Description: "Hostname (fqdn - test.example.ch) for the new alias.",
		},
		"hostname": {
			Type:        schema.TypeString,
			Required:    true,
			ForceNew:    true,
			Description: "Hostname (fqdn - hostname.example.ch) to which the alias should point.",
		},
		"ttl": {
			Type:        schema.TypeInt,
			Optional:    true,
			Default:     3600,
			ForceNew:    true,
			Description: "Set the TTL of the forward Record in seconds. Default is based on Zone TTL.",
		},
		"enable_external_view": {
			Type:        schema.TypeBool,
			Optional:    true,
			ForceNew:    true,
			Default:     true,
			Description: "Set true, if you want the Record to be avaiable outside of ETH. Default true",
		},
		"enable_internal_view": {
			Type:        schema.TypeBool,
			Optional:    true,
			Default:     true,
			ForceNew:    true,
			Description: "Set true, if you want the Record to be avaiable inside of ETH. Default True",
		},
		"isg_group": {
			Type:        schema.TypeString,
			Required:    true,
			ForceNew:    true,
			Description: "Set NetSupport group for the record. You must be a member of this group.",
		},
		"remark": {
			Type:        schema.TypeString,
			Computed:    true,
			Description: "Remark (Bemerkung) on Alias Record.",
		},
		"acme_base_url": {
			Type:        schema.TypeString,
			Optional:    true,
			ForceNew:    true,
			Default:     "https://acme.ethz.ch",
			Description: "Base URL for the ETH Let's Encrypt implementation.",
		},
		"acme_username": {
			Type:        schema.TypeString,
			Computed:    true,
			Description: "Acme Username as defined when Let's Encrypt is enabled in Netcenter.",
		},
		"acme_password": {
			Type:        schema.TypeString,
			Computed:    true,
			Description: "Acme Password as defined when Let's Encrypt is enabled in Netcenter.",
			Sensitive:   true,
		},
		"acme_subdomain": {
			Type:        schema.TypeString,
			Computed:    true,
			Description: "Acme Subdomain as defined when Let's Encrypt is enabled in Netcenter.",
		},
		"last_updated": {
			Type:     schema.TypeString,
			Computed: true,
		},
	}
}
