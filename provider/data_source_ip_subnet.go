package provider

import (
	"context"

	"github.com/hashicorp/terraform-plugin-sdk/v2/diag"
	"github.com/hashicorp/terraform-plugin-sdk/v2/helper/schema"
)

// Define the Schema of a NameToIp Record - also used in terraform tf files.
func dataSourceNetcenterIPSubnet() *schema.Resource {
	return &schema.Resource{
		// This description is used by the documentation generator and the language server.
		Description: "Sample data source in the Terraform provider scaffolding.",

		ReadContext: dataSourceNetcenterIPSubnetRead,

		Schema: map[string]*schema.Schema{
			"fqname": {
				// Get a NameToIp Record based on the hostname (fqdn Format). The Netcenter API can return one or more Records.
				Description: "Provide a fully-qualified hostname, e.g. host.example.com",
				Type:        schema.TypeString,
				Required:    true,
			},
			"entries": {
				Type:        schema.TypeList,
				Computed:    true,
				Description: "List of Host entries from the Netcenter API.",
				Elem: &schema.Resource{
					Schema: map[string]*schema.Schema{
						"id": {
							Type:        schema.TypeInt,
							Computed:    true,
							Description: "Terraform generated id for each entry.",
						},
						"fqname": {
							Type:        schema.TypeString,
							Computed:    true,
							Description: "Fully Qualified Hostname - FQName in Netcenter",
						},
						"ip": {
							Type:        schema.TypeString,
							Computed:    true,
							Description: "IP Address of NameToIp Entry",
						},
						"ipversion": {
							Type:        schema.TypeString,
							Computed:    true,
							Description: "IP Version (ipv4 or ipv6)",
						},
						"forward_enabled": {
							Type:        schema.TypeString,
							Computed:    true,
							Description: "If a Forward DNS Record is created or not",
						},
						"reverse_enabled": {
							Type:        schema.TypeString,
							Computed:    true,
							Description: "If a Reverse DNS Record is created or not",
						},
						"ttl": {
							Type:        schema.TypeInt,
							Computed:    true,
							Description: "Time to live of DNS Record.",
						},
						"dhcp_enabled": {
							Type:        schema.TypeBool,
							Computed:    true,
							Description: "If DHCP Reservation is enabled",
						},
						"dhcp_mac": {
							Type:        schema.TypeBool,
							Computed:    true,
							Description: "Mac Address for dhcp reservation. Empty if dhcp_enabled is false.",
						},
						"last_detection": {
							Type:        schema.TypeString,
							Computed:    true,
							Description: "Date and Time when the IP was last detected on the network.",
						},
						"netsupport_group": {
							Type:        schema.TypeString,
							Computed:    true,
							Description: "Netsupport Group the entry belongs to.",
						},
					},
				},
			},
		},
	}
}

// Function that retrieves NameToIp Entries based on a hostname (fqdn-style)
func dataSourceNetcenterIPSubnetRead(ctx context.Context, d *schema.ResourceData, meta interface{}) diag.Diagnostics {
	// client := meta.(*nc.Client)

	// Warning and error collector.
	var diags diag.Diagnostics

	// nameToIpHostname := d.Get("fqname").(string)

	// response, err := client.GetNameToIpRecord(nameToIpHostname)
	// if err != nil {
	// 	return diag.FromErr(err)
	// }

	// entries := make([]map[string]interface{}, 0)

	// for _, v := range response.IPs {
	// 	entry := make(map[string]interface{})

	// 	entry["id"] =

	// }

	idFromAPI := "my-id"
	d.SetId(idFromAPI)

	return diags
}
