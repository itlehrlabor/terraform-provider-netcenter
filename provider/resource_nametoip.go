package provider

import (
	"context"
	"fmt"
	"strings"
	"time"

	"github.com/hashicorp/terraform-plugin-log/tflog"
	"github.com/hashicorp/terraform-plugin-sdk/v2/diag"
	"github.com/hashicorp/terraform-plugin-sdk/v2/helper/schema"
	netcenter "gitlab.ethz.ch/itlehrlabor/netcenter-client-go/api"
)

func contains(s []string, str string) bool {
	for _, v := range s {
		if v == str {
			return true
		}
	}

	return false
}

func resourceNetcenterRecord() *schema.Resource {
	return &schema.Resource{
		CreateContext: resourceNetcenterRecordCreate,
		ReadContext:   resourceNetcenterRecordRead,
		//UpdateContext: resourceNetcenterRecordUpdate,
		DeleteContext: resourceNetcenterRecordDelete,
		SchemaVersion: 1,
		Schema:        resourceNetcenterNameToIpRecordSchema(),
		Importer: &schema.ResourceImporter{
			StateContext: resourceNetcenterRecordImport,
		},
	}
}

func resourceNetcenterRecordCreate(ctx context.Context, d *schema.ResourceData, m interface{}) diag.Diagnostics {
	// Warning or errors can be collected in a slice type
	client := m.(*netcenter.Client)

	tflog.Debug(ctx, fmt.Sprintf("Creating NameToIp Record: %s, %s", d.Get("name").(string), d.Get("ip_address").(string)))
	ipVersion := d.Get("ipversion").(string)
	// This is dumb but I don't see another option.
	tflog.Debug(ctx, "Checking booleans")
	forwardDecision := func() string {
		if d.Get("forward_dns").(bool) {
			return "Y"
		} else {
			return "N"
		}
	}
	reverseDecision := func() string {
		if d.Get("reverse_dns").(bool) {
			return "Y"
		} else {
			return "N"
		}
	}
	dhcpOption := func() string {
		if d.Get("enable_dhcp").(bool) {
			return "Y"
		} else {
			return "N"
		}
	}
	// Check what view is enabled and configure the View Array
	views := []string{}
	if d.Get("enable_external_view").(bool) {
		views = append(views, "extern")
	}
	if d.Get("enable_internal_view").(bool) {
		views = append(views, "intern")
	}

	if ipVersion == "v4" {
		tflog.Debug(ctx, "Detected IPv4 Address")
		newRecord := netcenter.IPv4InsertRecord{
			IP:       d.Get("ip_address").(string),
			Forward:  forwardDecision(),
			Reverse:  reverseDecision(),
			FqName:   d.Get("name").(string),
			Ttl:      d.Get("ttl").(int),
			Dhcp:     dhcpOption(),
			Views:    views,
			IsgGroup: d.Get("isg_group").(string),
		}

		if d.Get("dhcp_mac_reservation") != nil {
			newRecord.DhcpMac = d.Get("dhcp_mac_reservation").(string)
		}
		tflog.Debug(ctx, "Sending IPv4 Record Creation to API")
		r, err := client.CreateNameToIPv4Record(newRecord)
		if err != nil {
			tflog.Debug(ctx, "API Request for IPv4 failed")
			return diag.FromErr(err)
		}
		tflog.Debug(ctx, "API Request finished")
		iD := strings.Join([]string{r.FqName, r.IP}, "|")
		d.SetId(iD)

		d.Set("last_updated", time.Now().Format("2006-01-02 15:04"))

	} else if ipVersion == "v6" {
		tflog.Debug(ctx, "Detected IPv6 Address")
		newRecord := netcenter.IPv6InsertRecord{
			IP:       d.Get("ip_address").(string),
			Forward:  forwardDecision(),
			Reverse:  reverseDecision(),
			FqName:   d.Get("name").(string),
			Ttl:      d.Get("ttl").(int),
			Dhcp:     dhcpOption(),
			Views:    views,
			IsgGroup: d.Get("isg_group").(string),
		}
		if d.Get("dhcp_mac_reservation") != nil {
			newRecord.DhcpMac = d.Get("dhcp_mac_reservation").(string)
		}
		tflog.Debug(ctx, "Sending IPv6 Record Creation to API")
		r, err := client.CreateNameToIPv6Record(newRecord)
		if err != nil {
			tflog.Debug(ctx, "API Request for IPv6 failed")
			return diag.FromErr(err)
		}
		tflog.Debug(ctx, "API Request finished")
		iD := strings.Join([]string{r.FqName, r.IP}, "|")
		d.SetId(iD)
		d.Set("last_updated", time.Now().Format("2006-01-02 15:04"))

	}
	tflog.Info(ctx, "Checking if Let's Encrypt should be enabled.")
	if d.Get("enable_letsencrypt").(bool) {
		r_acme, err := client.EnableLetsEncrypt(d.Get("name").(string), "nameToIp")
		if err != nil {
			tflog.Debug(ctx, "API Request to enable Let's Encrypt failed.")
			return diag.FromErr(err)
		}
		d.Set("acme_base_url", r_acme.AcmeBaseUrl)
	}

	resourceNetcenterRecordRead(ctx, d, m)
	return nil
}

func resourceNetcenterRecordRead(ctx context.Context, d *schema.ResourceData, m interface{}) diag.Diagnostics {

	client := m.(*netcenter.Client)
	// Warning or errors can be collected in a slice type
	var diags diag.Diagnostics
	hostName := d.Get("name").(string)
	ipAddress := d.Get("ip_address").(string)

	tflog.Debug(ctx, fmt.Sprintf("Looking for Records for host: %s with IP: %s", hostName, ipAddress))
	records, err := client.GetNameToIpRecord(hostName)
	if err != nil {
		return diag.FromErr(err)
	}
	tflog.Debug(ctx, fmt.Sprintf("We found the following number of NameToIp entries in Netcenter: %d", len(records.IPs)))
	element := 0
	found := false

	if len(records.IPs) == 0 {
		tflog.Error(ctx, fmt.Sprintf("No Record in Netcenter found with hostname %s and IP Address %s. Destroying it in Terraform state. Maybe it was deleted manually?", hostName, ipAddress))
		d.SetId("")
		return diag.Errorf("No resource based on %s and %s found, maybe it was deleted manually.", hostName, ipAddress)
	}

	if len(records.IPs) > 1 {
		// We found multiple netcenter records, trying to find the right one based on ip Address
		for id, val := range records.IPs {
			tflog.Info(ctx, fmt.Sprintf("Found Record in List for %s and IP Address %s", val.FqName, val.IP))
			if val.IP == ipAddress {
				element = id
				found = true
			}
		}
		if !found {
			tflog.Error(ctx, fmt.Sprintf("no Record in Netcenter found with the hostname %s and IP Address %s", hostName, ipAddress))
			return diag.Errorf("No resource based on %s and %s found", hostName, ipAddress)
		}
	}

	iD := strings.Join([]string{records.IPs[element].FqName, records.IPs[element].IP}, "|")
	d.SetId(iD)
	d.Set("name", records.IPs[element].FqName)
	d.Set("ipversrion", records.IPs[element].IpVersion)
	d.Set("ip_address", records.IPs[element].IP)
	d.Set("ttl", records.IPs[element].TTL)
	// if records.IPs[element].DhcpEnabled == Y -> set d.Set("enable_dhcp" == true else false
	d.Set("dhcp_mac_reservation", records.IPs[element].DhcpMac)
	d.Set("isg_group", records.IPs[element].IsgGroup)
	d.Set("ip_last_detection", records.IPs[element].LastDetection)
	if d.Get("enable_letsencrypt").(bool) && d.Get("acme_base_url") != "" {
		d.Set("acme_username", records.IPs[element].AcmeUsername)
		d.Set("acme_password", records.IPs[element].AcmePassword)
		d.Set("acme_subdomain", records.IPs[element].AcmeSubdomain)
	}

	// Check if Forward DNS is enabled on NameToIP Element in Netcenter and set the appropriate value in TF state.
	if records.IPs[element].Forward == "Y" {
		d.Set("forward_dns", true)
	} else {
		d.Set("forward_dns", false)
	}
	// Check if Reverse DNS is enabled on NameToIP Element in Netcenter and set the appropriate value in TF state.
	if records.IPs[element].Reverse == "Y" {
		d.Set("reverse_dns", true)
	} else {
		d.Set("reverse_dns", false)
	}
	// Check if DHCP is enabled on NameToIP Element in Netcenter and set the appropriate value in TF state.
	if records.IPs[element].DhcpEnabled == "Y" {
		d.Set("enable_dhcp", true)
	} else {
		d.Set("enable_dhcp", false)
	}
	// Check what view is enabled and configure the View Array
	if contains(records.IPs[element].Views.Views, "extern") {
		d.Set("enable_external_view", true)
	} else {
		d.Set("enable_external_view", false)
	}
	if contains(records.IPs[element].Views.Views, "intern") {
		d.Set("enable_internal_view", true)
	} else {
		d.Set("enable_internal_view", false)
	}

	return diags
}

func resourceNetcenterRecordUpdate(ctx context.Context, d *schema.ResourceData, m interface{}) diag.Diagnostics {

	return resourceNetcenterRecordRead(ctx, d, m)
}

func resourceNetcenterRecordDelete(ctx context.Context, d *schema.ResourceData, m interface{}) diag.Diagnostics {

	client := m.(*netcenter.Client)

	hostName := d.Get("name").(string)
	ipAddress := d.Get("ip_address").(string)
	enabledLe := d.Get("enable_letsencrypt").(bool)
	// Checking if Let's Encrypt is enabled, if so, disable it.
	if enabledLe && d.Get("acme_base_url") != "" {
		tflog.Info(ctx, fmt.Sprintf("Disabling Let's Encrypt on Host %s with IP Address %s", hostName, ipAddress))
		_, err := client.DisableLetsEncrypt(hostName, "nameToIp")
		if err != nil {
			return diag.FromErr((fmt.Errorf("error deleting Let's Encrypt. Error: %w", err)))
		}
	}

	tflog.Info(ctx, fmt.Sprintf("Deleting Netcenter NameToIp Record: %s, %s", hostName, ipAddress))
	err := client.DeleteNameToIPRecord(ipAddress, hostName)
	if err != nil {
		return diag.FromErr(fmt.Errorf("error deleting Netcenter NameToIp Record: %w", err))
	}

	return nil

}

func resourceNetcenterRecordImport(ctx context.Context, d *schema.ResourceData, m interface{}) ([]*schema.ResourceData, error) {

	client := m.(*netcenter.Client)
	idElement := strings.Split(d.Id(), "|")
	var hostName string
	var ipAddress string
	if len(idElement) == 2 {
		hostName = idElement[0]
		ipAddress = idElement[1]
	} else {
		return nil, fmt.Errorf("invalid id %q specified, should be in format \"hostname|ip_address\" for import", d.Id())
	}

	records, err := client.GetNameToIpIpRecord(ipAddress)
	if err != nil {
		return nil, fmt.Errorf("unable to find record with hostname %s and IP %s. Reason: %w", hostName, ipAddress, err)
	}
	tflog.Info(ctx, fmt.Sprintf("Found %d Records.", len(records.IPs)))

	found := false
	element := 0

	for id, val := range records.IPs {
		tflog.Info(ctx, fmt.Sprintf("Found Record in List for %s and IP Address %s", val.FqName, val.IP))
		if val.IP == ipAddress {
			element = id
			found = true
			break
		}
	}
	if !found {
		tflog.Error(ctx, fmt.Sprintf("no Record in Netcenter found with the hostname %s and IP Address %s", hostName, ipAddress))
		return nil, fmt.Errorf("no resource based on %s and %s found", hostName, ipAddress)
	}

	d.Set("name", records.IPs[element].FqName)
	d.Set("ip_address", records.IPs[element].IP)
	d.Set("ipversion", records.IPs[element].IpVersion)
	d.Set("isg_group", records.IPs[element].IsgGroup)

	resourceNetcenterRecordRead(ctx, d, m)

	return []*schema.ResourceData{d}, nil
}
