package provider

import (
	"os"
	"testing"

	"github.com/hashicorp/terraform-plugin-sdk/v2/helper/schema"
)

// providerFactories are used to instantiate a provider during acceptance testing.
// The factory function will be invoked for every Terraform CLI command executed
// to create a provider server to which the CLI can reattach.
var providerFactories = map[string]func() (*schema.Provider, error){
	"netcenter": func() (*schema.Provider, error) {
		return Provider(), nil
	},
}

var testAccProviders map[string]*schema.Provider
var testAccProvider *schema.Provider

func init() {
	testAccProvider = Provider()
	testAccProviders = map[string]*schema.Provider{
		"hashicups": testAccProvider,
	}
}

func TestProvider(t *testing.T) {
	if err := Provider().InternalValidate(); err != nil {
		t.Fatalf("err: %s", err)
	}
}

func TestProvider_impl(t *testing.T) {
	var _ *schema.Provider = Provider()
}

func testAccPreCheck(t *testing.T) {
	if err := os.Getenv("NETCENTER_USERNAME"); err == "" {
		t.Fatal("NETCENTER_USERNAME must be set for acceptance tests")
	}
	if err := os.Getenv("NETCENTER_PASSWORD"); err == "" {
		t.Fatal("NETCENTER_PASSWORD must be set for acceptance tests")
	}
	if err := os.Getenv("NETCENTER_HOSTNAME"); err == "" {
		t.Fatal("NETCENTER_HOSTNAME must be set for acceptance tests")
	}
}
