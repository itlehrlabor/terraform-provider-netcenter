package provider

import (
	"context"
	"fmt"
	"strings"
	"time"

	"github.com/hashicorp/terraform-plugin-log/tflog"
	"github.com/hashicorp/terraform-plugin-sdk/v2/diag"
	"github.com/hashicorp/terraform-plugin-sdk/v2/helper/schema"
	netcenter "gitlab.ethz.ch/itlehrlabor/netcenter-client-go/api"
)

func resourceNetcenterAliasRecord() *schema.Resource {
	return &schema.Resource{
		CreateContext: resourceNetcenterAliasRecordCreate,
		ReadContext:   resourceNetcenterAliasRecordRead,
		//UpdateContext: resourceNetcenterAliasRecordUpdate,
		DeleteContext: resourceNetcenterAliasRecordDelete,
		SchemaVersion: 1,
		Schema:        resourceNetcenterAliasRecordSchema(),
		Importer: &schema.ResourceImporter{
			StateContext: resourceNetcenterAliasRecordImport,
		},
	}
}

func resourceNetcenterAliasRecordCreate(ctx context.Context, d *schema.ResourceData, m interface{}) diag.Diagnostics {
	// Warning or errors can be collected in a slice type
	client := m.(*netcenter.Client)

	tflog.Debug(ctx, fmt.Sprintf("Creating Alias Record: %s, %s", d.Get("alias_name").(string), d.Get("hostname").(string)))
	// Check what view is enabled and configure the View Array
	views := []string{}
	if d.Get("enable_external_view").(bool) {
		views = append(views, "extern")
	}
	if d.Get("enable_internal_view").(bool) {
		views = append(views, "intern")
	}

	tflog.Debug(ctx, "Creating Record.")
	newRecord := netcenter.AliasInsertRecord{
		FqName:      d.Get("alias_name").(string),
		HostName:    d.Get("hostname").(string),
		Ttl:         d.Get("ttl").(int),
		NetsupGroup: d.Get("isg_group").(string),
		Views:       views,
	}
	tflog.Debug(ctx, "Sending Alias Record Creation to API")
	r, err := client.CreateAliasRecord(newRecord)
	if err != nil {
		tflog.Debug(ctx, "API Request for Alias Record failed")
		return diag.FromErr(err)
	}
	tflog.Debug(ctx, "API Request successfully finished")
	iD := strings.Join([]string{r.FqName, r.HostName}, "|")
	d.SetId(iD)

	d.Set("last_updated", time.Now().Format("2006-01-02 15:04"))

	resourceNetcenterAliasRecordRead(ctx, d, m)
	return nil
}

func resourceNetcenterAliasRecordRead(ctx context.Context, d *schema.ResourceData, m interface{}) diag.Diagnostics {

	client := m.(*netcenter.Client)
	// Warning or errors can be collected in a slice type
	var diags diag.Diagnostics
	aliasNAme := d.Get("alias_name").(string)
	fqName := d.Get("hostname").(string)

	tflog.Debug(ctx, fmt.Sprintf("Looking for Alias Records for host: %s with IP: %s", aliasNAme, fqName))
	records, err := client.GetAliasRecord(aliasNAme)
	if err != nil {
		return diag.FromErr(err)
	}
	tflog.Debug(ctx, fmt.Sprintf("We found the following number of Alias entries in Netcenter: %d", len(records.Aliases)))
	element := 0
	found := false

	if len(records.Aliases) == 0 {
		tflog.Error(ctx, fmt.Sprintf("No Record in Netcenter found with hostname %s and IP Address %s. Destroying it in Terraform state. Maybe it was deleted manually?", aliasNAme, fqName))
		d.SetId("")
		return diag.Errorf("No resource based on %s and %s found, maybe it was deleted manually.", aliasNAme, fqName)
	}

	if len(records.Aliases) > 1 {
		// We found multiple netcenter records, trying to find the right one based on ip Address
		for id, val := range records.Aliases {
			tflog.Info(ctx, fmt.Sprintf("Found Record in List for Alias %s and Hostname %s", val.HostName, val.HostName))
			if val.HostName == fqName {
				element = id
				found = true
			}
		}
		if !found {
			tflog.Error(ctx, fmt.Sprintf("no Record in Netcenter found with the Alias %s and Hostname %s", aliasNAme, fqName))
			return diag.Errorf("No resource based on %s and %s found", aliasNAme, fqName)
		}
	}

	iD := strings.Join([]string{records.Aliases[element].FqName, records.Aliases[element].HostName}, "|")
	d.SetId(iD)
	d.Set("alias_name", records.Aliases[element].FqName)
	d.Set("hostname", records.Aliases[element].HostName)
	d.Set("ttl", records.Aliases[element].Ttl)
	d.Set("isg_group", records.Aliases[element].NetsupGroup)
	d.Set("remark", records.Aliases[element].Remark)
	if records.Aliases[element].AcmeSubdomain != "" {
		d.Set("acme_username", records.Aliases[element].AcmeUsername)
		d.Set("acme_password", records.Aliases[element].AcmePassword)
		d.Set("acme_subdomain", records.Aliases[element].AcmeSubdomain)
	} else {
		d.Set("acme_username", "")
		d.Set("acme_password", "")
		d.Set("acme_subdomain", "")
	}
	// Check what view is enabled and configure the View Array
	if contains(records.Aliases[element].Views.Views, "extern") {
		d.Set("enable_external_view", true)
	} else {
		d.Set("enable_external_view", false)
	}
	if contains(records.Aliases[element].Views.Views, "intern") {
		d.Set("enable_internal_view", true)
	} else {
		d.Set("enable_internal_view", false)
	}

	return diags
}

func resourceNetcenterAliasRecordUpdate(ctx context.Context, d *schema.ResourceData, m interface{}) diag.Diagnostics {

	return resourceNetcenterAliasRecordRead(ctx, d, m)
}

func resourceNetcenterAliasRecordDelete(ctx context.Context, d *schema.ResourceData, m interface{}) diag.Diagnostics {

	client := m.(*netcenter.Client)

	aliasName := d.Get("alias_name").(string)
	hostName := d.Get("hostname").(string)
	// Checking if Let's Encrypt is enabled, if so, disable it.
	if d.Get("acme_username") != "" {
		tflog.Error(ctx, fmt.Sprintf("Let's Encrypt is enabled for Alias %s with Hostname %s. Please manually disable it in Netcenter, before terraform destroy.", aliasName, hostName))
		return diag.FromErr((fmt.Errorf("error, cannot Delete Let's Encrypt from Alias. Please delete Let's Encrypt on Alias manually before and refresh state")))
	}

	tflog.Info(ctx, fmt.Sprintf("Deleting Netcenter Alias Record: %s, %s", aliasName, hostName))
	err := client.DeleteAliasRecord(aliasName)
	if err != nil {
		return diag.FromErr(fmt.Errorf("error deleting Netcenter NameToIp Record: %w", err))
	}

	return nil

}

func resourceNetcenterAliasRecordImport(ctx context.Context, d *schema.ResourceData, m interface{}) ([]*schema.ResourceData, error) {

	client := m.(*netcenter.Client)
	idElement := strings.Split(d.Id(), "|")
	var aliasName string
	var hostName string
	if len(idElement) == 2 {
		aliasName = idElement[0]
		hostName = idElement[1]
	} else {
		return nil, fmt.Errorf("invalid id %q specified, should be in format \"alias_name|hostname\" for import", d.Id())
	}

	records, err := client.GetAliasRecord(aliasName)
	if err != nil {
		return nil, fmt.Errorf("unable to find record with Alias Name %s. Reason: %w", aliasName, err)
	}
	tflog.Info(ctx, fmt.Sprintf("Found %d Records.", len(records.Aliases)))

	found := false
	element := 0

	for id, val := range records.Aliases {
		tflog.Info(ctx, fmt.Sprintf("Found Record in List for %s and Hostname %s", val.FqName, val.HostName))
		if val.HostName == hostName {
			element = id
			found = true
			break
		}
	}
	if !found {
		tflog.Error(ctx, fmt.Sprintf("no Record in Netcenter found with the Alias Name %s and Hostname %s", aliasName, hostName))
		return nil, fmt.Errorf("no resource based on %s and %s found", aliasName, hostName)
	}

	d.Set("alias_name", records.Aliases[element].FqName)
	d.Set("hostname", records.Aliases[element].HostName)

	resourceNetcenterRecordRead(ctx, d, m)

	return []*schema.ResourceData{d}, nil
}
